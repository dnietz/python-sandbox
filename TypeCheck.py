'''
Created on Apr 7, 2014

@author: dnietz

Written for Python 3.4
'''

import unittest

def typechecker(func):
    functionname = func.__name__
    returnisannotated = False
    returntypename = None
    paramtypenames = dict()
    
    #record the types we should get according to the function's annotations
    if func.__annotations__:
        #first the parameters
        for param in func.__annotations__.keys():
            if not param == "return":
                paramtypenames[param]=func.__annotations__[param].__name__
        #then the return type
        if "return" in func.__annotations__.keys():
            returnisannotated = True
            returntypename = func.__annotations__["return"].__name__
    else: #if there are no annotations, why do this?
        raise TypeError("no function annotations present in %s" % functionname)
    
    #when we run the function, we'll see what we actually get
    def wrapper(**kwargs):
        #first check the types of the arguments passed
        if kwargs:
            for argnametocheck in kwargs.keys():
                if not type(kwargs[argnametocheck]).__name__ == paramtypenames[argnametocheck]:
                    raise TypeError("%s's param %s was type %s when it should be %s" % (functionname, argnametocheck, type(kwargs[argnametocheck]).__name__, paramtypenames[argnametocheck]))
        #then run the function and check the type of the result
        result = func(**kwargs)
        if returnisannotated:
            if not type(result).__name__ == returntypename:
                raise TypeError("%s doesn't return type %s, instead it is %s" % (functionname, returntypename, type(result).__name__))
        return result
    
    return wrapper

@typechecker
def functionundertest(number: int=0) -> int:
    return number + 1

@typechecker
def functionwrongreturntype() -> bool:
    return 23

@typechecker
def functionkwargs(theword: str="thebird", theloneliestnumber: int=1):
    if theloneliestnumber == 1:
        return 0
    if not theword == "thebird":
        return "ok fine, I guess the word is %s" % theword
    return theloneliestnumber + 1

@typechecker
def argandkwarg(name: str="", number: int=0) -> str:
    return name + str(number)

@typechecker
def whatisyourname(name: str="Fred"):
    print("Hi, %s" % name)

class testtypechecking(unittest.TestCase):
    
    def testfunctionundertestwithint(self):
        result = functionundertest(number=1)
        self.assertEqual(result, 2, "sent 1, should have gotten 2")
         
    def testfunctionundertestwithbool(self):
        self.assertRaises(TypeError, functionundertest, True)
         
    def testfunctionwrongreturntype(self):
        self.assertRaises(TypeError, functionwrongreturntype)
     
    def testfunctionkwargs(self):
        self.assertEqual(functionkwargs(theloneliestnumber=1, theword="bird"), 0, "gave it 1, it should have returned 0")
         
    def testwhatisyourname(self):
        self.assertEqual(whatisyourname(name="Brunhilde"), None, "whatisyourname should return None cuz there's no return")
        
    def testargandkwarg(self):
        self.assertEqual(argandkwarg(name="Parsifal"), "Parsifal0", "Number should have defaulted to 0")
        self.assertEqual(argandkwarg(name="Lohengrin", number=1), "Lohengrin1", "Number should be 1, which was passed in")
    
if __name__ == "__main__":
    unittest.main(verbosity=2)

